A collection of scripts and fonts made in and for [Manufactura
Independente](http://manufacturaindependente.org)'s bitmap type design
workshops.

TODO: Document each script, font and repo structure

Edit in:
  * fonts/Avestruz/glyphs
  * fonts/Flamingo/glyphs
  * fonts/Tucan/glyphs

List of characters:

    A : aCap.txt
    B : bCap.txt
    C : cCap.txt
    D : dCap.txt
    E : eCap.txt
    F : fCap.txt
    G : gCap.txt
    H : hCap.txt
    I : iCap.txt
    J : jCap.txt
    K : kCap.txt
    L : lCap.txt
    M : mCap.txt
    N : nCap.txt
    O : oCap.txt
    P : pCap.txt
    Q : qCap.txt
    R : rCap.txt
    S : sCap.txt
    T : tCap:.txt
    U : uCap.txt
    V : vCap.txt
    W : wCap.txt
    X : xCap.txt
    Y : yCap.txt
    Z : zCap.txt
    -----
    1.txt
    2.txt
    3.txt
    4.txt
    5.txt
    6.txt
    7.txt
    8.txt
    9.txt
    0.txt
    -----
    interrogación        - ? : _questionmark.txt
    exclamación          - ! : _exclamationmark.txt
    paréntesis izquierdo - ( : _leftparenthesis.txt
    paréntesis derecho   - ) : _rightparenthesis.txt
    llave izquierda      - { : _leftcurlybracket.txt
    llave derecha        - } : _rightcurlybracket.txt
    corchete izquierdo   - [ : _leftsquarebracket.txt
    corchete derecho     - ] : _rightsquarebracket.txt 
    arroba               - @ : _commercialat.txt
    dólar                - $ : _dollarsign.txt
    euro                 - € : _euro.txt
    almohadilla          - # : _numbersign.txt
    et                   - & : _ampersand.txt
    comillas             - " : _quotationmark.txt
    asterisco            - * : _asterisk.txt
    guion/menos          - - : _hyphenminus.txt
    más                  - + : _plussign.txt
    barra                - / : _solidus.txt
    punto                - . : _fullstop.txt
    coma                 - , : _comma.txt
    dos puntos           - : : _colon.txt
    punto y coma         - ; : _semicolon.txt
    porcentaje           - % : _percentsign.txt
